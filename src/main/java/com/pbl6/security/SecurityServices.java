package com.pbl6.security;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Date;

@Service
public class SecurityServices {

    @Value("${security.jwt.prefix}")
    private String PREFIX;

    @Value("${security.jwt.secret-key}")
    private String SECRET_KEY;

    @Value("${security.jwt.expire-length}")
    private int JWT_EXPIRE_TIME;

    private final UserDetailsServiceImpl userDetailsService;

    @Autowired
    public SecurityServices(UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @PostConstruct
    protected void initialize() {
        this.SECRET_KEY = Base64.getEncoder().encodeToString(SECRET_KEY.getBytes());
    }

    public String createAccessJwt(String email) {
        Date now = new Date();
        Date expireDate = new Date(now.getTime() + this.JWT_EXPIRE_TIME);

        return Jwts.builder()
                .setIssuedAt(now)
                .setSubject(email)
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS256, this.SECRET_KEY)
                .compact();
    }

    public String getSubject(String jwt) {

        try {
            return Jwts.parser()
                    .setSigningKey(this.SECRET_KEY)
                    .parseClaimsJws(jwt)
                    .getBody()
                    .getSubject();

        } catch (ExpiredJwtException e) {
            return e.getClaims().getSubject();
        }
    }

    public boolean isValidJwt(String jwt) {
        try {
            Jwts.parser()
                    .setSigningKey(this.SECRET_KEY)
                    .parseClaimsJws(jwt);
            return true;

        } catch (ExpiredJwtException e) {
            return false;

        } catch (JwtException | IllegalArgumentException e) {
            throw new RuntimeException("Invalid JWT");
        }
    }


    public Authentication getAuthentication(String jwt) {
        String email = getSubject(jwt);
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(email);

        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public String getJwtFromRequest(HttpServletRequest req) {
        String bearerToken = req.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith(this.PREFIX + " ")) {
            return bearerToken.replace(this.PREFIX + " ", "");
        }
        return null;
    }

}

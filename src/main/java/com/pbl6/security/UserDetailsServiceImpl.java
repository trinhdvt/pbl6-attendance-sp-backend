package com.pbl6.security;


import com.pbl6.model.entity.User;
import com.pbl6.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {


    private final UserRepository userRepo;

    public UserDetailsServiceImpl(UserRepository userRepo) {
        this.userRepo = userRepo;

    }

    @Override
    public UserDetails loadUserByUsername(java.lang.String email) throws UsernameNotFoundException {
        User teacher = userRepo.findByEmail(email);

        if (teacher == null) {
            throw new UsernameNotFoundException("Wrong email or password");
        }

        return new org.springframework.security.core.userdetails.User(teacher.getEmail(),
                teacher.getPassword(),
                Collections.singletonList(teacher.getRole()));
    }
}

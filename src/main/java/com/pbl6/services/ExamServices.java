package com.pbl6.services;

import com.pbl6.exception.HttpError;
import com.pbl6.exception.NotFoundError;
import com.pbl6.model.entity.Examination;
import com.pbl6.model.entity.User;
import com.pbl6.model.request.ExamPayload;
import com.pbl6.model.response.ExamDTO;
import com.pbl6.repository.ExamRepository;
import com.pbl6.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class ExamServices {

    private final ExamRepository examRepository;

    private final UserRepository userRepo;

    public List<ExamDTO> getAllExams(Pageable pageable) {
        return examRepository.findAll(pageable)
                .map(ExamDTO::toExamDTO)
                .getContent();
    }

    public ExamDTO createExam(ExamPayload examPayload, String teacherEmail) {
        // check if exam code exists
        if (examRepository.findByExamCode(examPayload.getExamCode()) != null) {
            throw new HttpError("Exam code already exists", HttpStatus.CONFLICT);
        }

        // if not then create new exam
        User teacher = userRepo.findByEmail(teacherEmail);
        Examination newExam = Examination.builder()
                .creator(teacher)
                .examCode(examPayload.getExamCode())
                .detail(examPayload.getDetails())
                .openAt(examPayload.getOpenAt())
                .closeAt(examPayload.getCloseAt())
                .build();

        // save then return as DTO
        newExam = examRepository.saveAndFlush(newExam);
        return ExamDTO.toExamDTO(newExam);
    }

    public ExamDTO editExam(ExamPayload examPayload, int id) {
        // check if id is already exists
        Examination exam = examRepository.findById(id).orElseThrow(() -> new NotFoundError("Exam not found"));

        // check if exam code is already exists
        Examination tmp = examRepository.findByExamCode(examPayload.getExamCode());
        if (tmp != null && tmp.getId() != id) {
            throw new HttpError("Exam code already exists", HttpStatus.CONFLICT);
        }

        // update exam
        exam.setExamCode(examPayload.getExamCode());
        exam.setDetail(examPayload.getDetails());
        exam.setOpenAt(examPayload.getOpenAt());
        exam.setCloseAt(examPayload.getCloseAt());

        exam = examRepository.saveAndFlush(exam);
        return ExamDTO.toExamDTO(exam);
    }
}

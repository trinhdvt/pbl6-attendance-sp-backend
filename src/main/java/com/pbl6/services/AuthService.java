package com.pbl6.services;

import com.pbl6.exception.UnauthorizedError;
import com.pbl6.model.entity.User;
import com.pbl6.model.request.LoginPayload;
import com.pbl6.repository.UserRepository;
import com.pbl6.security.SecurityServices;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class AuthService {

    private final AuthenticationManager authManager;

    private final SecurityServices securityServices;

    private final UserRepository userRepo;

    @Value("${security.refresh-token.cookie-name}")
    private String REFRESH_TOKEN_COOKIE_NAME;

    @Value("${security.refresh-token.cookie-length}")
    private int REFRESH_TOKEN_COOKIE_LENGTH;

    public String login(LoginPayload loginPayload, HttpServletResponse resp) {
        try {
            authManager.authenticate(new UsernamePasswordAuthenticationToken(loginPayload.getEmail(), loginPayload.getPassword()));

            String accessToken = securityServices.createAccessJwt(loginPayload.getEmail());
            addRefreshTokenCookie(loginPayload.getEmail(), accessToken, resp);

            return accessToken;

        } catch (AuthenticationException e) {
            throw new UnauthorizedError("Invalid email / password");
        }
    }

    private void addRefreshTokenCookie(String email, String accessToken, HttpServletResponse resp) {
        String refreshToken = createRefreshToken(email, accessToken);

        Cookie cookie = new Cookie(REFRESH_TOKEN_COOKIE_NAME, refreshToken);
        cookie.setHttpOnly(true);
        cookie.setMaxAge(REFRESH_TOKEN_COOKIE_LENGTH); // ms -> s
        cookie.setPath("/");
        resp.addCookie(cookie);
    }

    private String createRefreshToken(String email, String accessToken) {
        String rfToken = UUID.randomUUID().toString();

        User user = userRepo.findByEmail(email);
        if (user != null) {
            user.setRefreshToken(rfToken);
            user.setAccessToken(accessToken);

            userRepo.saveAndFlush(user);
            return rfToken;
        } else {
            throw new UnauthorizedError("Not found user");
        }


    }
}

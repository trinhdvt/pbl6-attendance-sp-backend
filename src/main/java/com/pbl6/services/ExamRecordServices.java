package com.pbl6.services;

import com.pbl6.exception.NotFoundError;
import com.pbl6.model.entity.AttendanceRecord;
import com.pbl6.model.entity.Examination;
import com.pbl6.model.request.AttendancePayload;
import com.pbl6.model.request.RecordPayload;
import com.pbl6.model.response.RecordDTO;
import com.pbl6.repository.AttendanceRepository;
import com.pbl6.repository.ExamRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class ExamRecordServices {

    private final ModelMapper modelMapper;

    private final AttendanceRepository attendanceRepo;

    private final ExamRepository examRepo;

    public List<RecordDTO> getRecord(int examId, Pageable pageable) {

        return attendanceRepo.findAllByExamination_Id(examId, pageable)
                .map(attendance -> modelMapper.map(attendance, RecordDTO.class))
                .getContent();
    }

    public RecordDTO modifyRecord(int recordId, RecordPayload payload) {
        AttendanceRecord record = attendanceRepo.findById(recordId).orElseThrow(() -> new NotFoundError("Record not found"));

        record.setStatus(payload.isStatus());
        record.setStudentId(payload.getStudentId());
        record.setStudentName(payload.getStudentName());
        record.setBirthday(payload.getBirthday());
        record.setFacultyName(payload.getFacultyName());
        record.setClassName(payload.getClassName());
        record.setYear(payload.getYear());

        record = attendanceRepo.saveAndFlush(record);
        return modelMapper.map(record, RecordDTO.class);
    }

    public RecordDTO submitRecord(AttendancePayload payload) {
        String examCode = payload.getExamCode();
        Examination examination = examRepo.findByExamCode(examCode);
        if (examination == null) {
            throw new NotFoundError("Exam not found");
        }

        AttendanceRecord record = modelMapper.map(payload, AttendanceRecord.class);
        record.setExamination(examination);
        record = attendanceRepo.saveAndFlush(record);

        return modelMapper.map(record, RecordDTO.class);
    }
}

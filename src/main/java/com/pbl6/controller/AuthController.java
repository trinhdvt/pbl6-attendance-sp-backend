package com.pbl6.controller;

import com.pbl6.model.request.LoginPayload;
import com.pbl6.services.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Collections;
import java.util.Map;

@RestController
@Validated
@RequestMapping("/auth")
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class AuthController {

    private final AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid LoginPayload loginPayload, HttpServletResponse resp) {
        String jwt = authService.login(loginPayload, resp);

        Map<String, String> response = Collections.singletonMap("token", jwt);
        return ResponseEntity.ok(response);
    }
}

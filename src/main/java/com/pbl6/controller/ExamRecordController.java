package com.pbl6.controller;

import com.pbl6.model.request.AttendancePayload;
import com.pbl6.model.request.RecordPayload;
import com.pbl6.model.response.RecordDTO;
import com.pbl6.services.ExamRecordServices;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Validated
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class ExamRecordController {

    private final ExamRecordServices attendanceServices;


    //    get all attendance record of exams
    @GetMapping("/exams/{examId}/records")
    @PreAuthorize("hasAuthority('TEACHER')")
    public ResponseEntity<?> getAttendance(@PathVariable("examId") int examId,
                                           @PageableDefault(sort = {"checkAt"},
                                                   direction = Direction.DESC) Pageable pageable) {

        List<RecordDTO> attendanceRecord = attendanceServices.getRecord(examId, pageable);
        return ResponseEntity.ok(attendanceRecord);
    }


    //    modify attendance record of exam
    @PutMapping("/records/{recordId}")
    @PreAuthorize("hasAuthority('TEACHER')")
    public ResponseEntity<?> modifyAttendance(@PathVariable("recordId") int recordId,
                                              @Valid RecordPayload payload) {

        RecordDTO record = attendanceServices.modifyRecord(recordId, payload);
        return ResponseEntity.ok(record);
    }

//    submit new record
    @PostMapping("/records")
    public ResponseEntity<?> submitRecord(@Valid AttendancePayload payload) {

        RecordDTO record = attendanceServices.submitRecord(payload);
        return ResponseEntity.ok(record);
    }
}

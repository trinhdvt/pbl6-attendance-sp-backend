package com.pbl6.controller;

import com.pbl6.exception.HttpError;
import com.pbl6.model.request.ExamPayload;
import com.pbl6.model.response.ExamDTO;
import com.pbl6.services.ExamServices;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Validated
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class ExamController {

    private final ExamServices examService;

    @GetMapping("/exams")
    @PreAuthorize("hasAuthority('TEACHER')")
    public ResponseEntity<?> getAllExams(@PageableDefault Pageable pageable) {

        List<ExamDTO> exams = examService.getAllExams(pageable);
        return ResponseEntity.ok(exams);
    }


    @PostMapping("/exams")
    @PreAuthorize("hasAuthority('TEACHER')")
    public ResponseEntity<?> createExam(@Valid ExamPayload examPayload, Authentication auth) {
        if (examPayload.getCloseAt().before(examPayload.getOpenAt())) {
            throw new HttpError("Close time must be after open time", HttpStatus.BAD_REQUEST);
        }

        String teacherEmail = auth.getName();
        ExamDTO examDTO = examService.createExam(examPayload, teacherEmail);

        return ResponseEntity.ok(examDTO);
    }

    @PutMapping("/exams/{id}")
    @PreAuthorize("hasAuthority('TEACHER')")
    public ResponseEntity<?> editExam(@Valid ExamPayload examPayload,
                                      @PathVariable int id) {
        if (examPayload.getCloseAt().before(examPayload.getOpenAt())) {
            throw new HttpError("Close time must be after open time", HttpStatus.BAD_REQUEST);
        }

        ExamDTO examDTO = examService.editExam(examPayload, id);
        return ResponseEntity.ok(examDTO);
    }
}

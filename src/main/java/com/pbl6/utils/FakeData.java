package com.pbl6.utils;

import com.github.javafaker.Faker;
import com.pbl6.model.entity.AttendanceRecord;
import com.pbl6.model.entity.Examination;
import com.pbl6.model.entity.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class FakeData {

    private static final Faker faker = new Faker(new Locale("en-US"));


    public static List<Examination> createSampleExam(User teacher, int amount) {

        List<Examination> list = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            String examCode = faker.educator().course();
            String detail = faker.lorem().characters(10, 20);
            Date now = new Date();
            Date close = faker.date().future(3, TimeUnit.HOURS);

            Examination ex = Examination.builder()
                    .examCode(examCode)
                    .detail(detail)
                    .creator(teacher)
                    .openAt(now)
                    .closeAt(close)
                    .build();

            list.add(ex);
        }

        return list;
    }


    public static List<AttendanceRecord> createSampleAttendance(int amount) {
        List<AttendanceRecord> list = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            AttendanceRecord ex = AttendanceRecord.builder()
                    .status(faker.bool().bool())
                    .studentId(faker.number().digits(9))
                    .studentName(faker.name().fullName())
                    .className(faker.commerce().department())
                    .facultyName(faker.commerce().department())
                    .birthday(faker.date().birthday(18, 25).toString())
                    .year(faker.number().numberBetween(2015, 2022) + "")
                    .faceImg(faker.internet().image())
                    .cardImg(faker.internet().image())
                    .cropCard(faker.internet().image())
                    .ipAddress(faker.internet().ipV4Address())
                    .userAgent(faker.internet().userAgentAny())
                    .build();

            list.add(ex);
        }
        return list;
    }
}

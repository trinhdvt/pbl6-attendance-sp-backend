package com.pbl6.exception;

import org.springframework.http.HttpStatus;

public class NotFoundError extends HttpError {

    public NotFoundError(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }
}

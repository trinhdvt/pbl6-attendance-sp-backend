package com.pbl6.repository;

import com.pbl6.model.entity.Examination;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExamRepository extends JpaRepository<Examination, Integer> {

    Examination findByExamCode(String examCode);

}

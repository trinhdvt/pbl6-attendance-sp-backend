package com.pbl6.repository;

import com.pbl6.model.entity.AttendanceRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttendanceRepository extends JpaRepository<AttendanceRecord, Integer> {

    Page<AttendanceRecord> findAllByExamination_Id(Integer id, Pageable pageable);
}

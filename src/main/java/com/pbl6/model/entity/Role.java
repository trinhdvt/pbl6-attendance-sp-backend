package com.pbl6.model.entity;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    TEACHER;

    @Override
    public String getAuthority() {
        return name();
    }
}

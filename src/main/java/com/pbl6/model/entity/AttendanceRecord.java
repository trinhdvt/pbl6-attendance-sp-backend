package com.pbl6.model.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Table(name = "pbl6_attendance_record")
@Entity
public class AttendanceRecord {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "exam_id")
    private Examination examination;

    @Builder.Default
    private boolean status = false;

    private Date checkAt;
    private String studentId;
    private String studentName;
    private String className;
    private String facultyName;
    private String birthday;
    private String year;
    private String ipAddress;
    private String faceImg;
    private String cardImg;
    private String cropCard;
    private String userAgent;


    @PrePersist
    void prePersist() {
        this.checkAt = new Date();
    }

    @Override
    public String toString() {
        return "AttendanceRecord{" +
                "id=" + id +
                ", examination=" + examination.getExamCode() +
                ", status=" + status +
                ", checkAt=" + checkAt +
                ", studentId='" + studentId + '\'' +
                ", studentName='" + studentName + '\'' +
                ", className='" + className + '\'' +
                ", facultyName='" + facultyName + '\'' +
                ", birthday='" + birthday + '\'' +
                ", year='" + year + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", faceImg='" + faceImg + '\'' +
                ", cardImg='" + cardImg + '\'' +
                ", cropCard='" + cropCard + '\'' +
                ", userAgent='" + userAgent + '\'' +
                '}';
    }
}

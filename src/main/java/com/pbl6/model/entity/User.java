package com.pbl6.model.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "pbl6_user")
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    private String email;

    private String password;

    private String fullName;

    private Date createdAt;

    private String refreshToken;

    private String accessToken;

    @Builder.Default
    @Enumerated(EnumType.STRING)
    private Role role = Role.TEACHER;

    @OneToMany(mappedBy = "creator")
    private List<Examination> createdExam = new ArrayList<>();

    @Override
    public String toString() {
        return "Admin{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", fullName='" + fullName + '\'' +
                ", createdAt=" + createdAt +
                ", refreshToken='" + refreshToken + '\'' +
                ", accessToken='" + accessToken + '\'' +
                '}';
    }
}

package com.pbl6.model.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Table(name = "pbl6_examination")
@Entity
public class Examination {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    private String examCode;

    private String detail;

    private Date openAt;

    private Date closeAt;

    @ManyToOne
    @JoinColumn(name = "admin_id")
    private User creator;

    @OneToMany(mappedBy = "examination")
    private List<AttendanceRecord> record = new ArrayList<>();

    @Override
    public String toString() {
        return "Examination{" +
                "id=" + id +
                ", examCode='" + examCode + '\'' +
                ", detail='" + detail + '\'' +
                ", openAt=" + openAt +
                ", closeAt=" + closeAt +
                ", creator=" + creator.getEmail() +
                ", record=" + record +
                '}';
    }
}

package com.pbl6.model.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class ExamPayload {

    @NotEmpty
    private String examCode;

    @NotEmpty
    private String details;

    @NotNull
    private Date openAt;

    @NotNull
    private Date closeAt;

}

package com.pbl6.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RecordPayload {
    private boolean status;
    private String studentId;
    private String studentName;
    private String className;
    private String facultyName;
    private String birthday;
    private String year;

}

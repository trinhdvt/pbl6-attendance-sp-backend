package com.pbl6.model.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class LoginPayload {

    @Email
    @NotEmpty
    private String email;

    @NotEmpty
    private String password;

}

package com.pbl6.model.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;

@EqualsAndHashCode(callSuper = true)
@Data
public class AttendancePayload extends RecordPayload {
    @NotEmpty
    private String examCode;
    private String ipAddress;
    private String faceImg;
    private String cardImg;
    private String cropCard;
    private String userAgent;
}

package com.pbl6.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RecordDTO {
    private Integer id;
    private boolean status;
    private Date checkAt;
    private String studentId;
    private String studentName;
    private String className;
    private String facultyName;
    private String birthday;
    private String year;
    private String ipAddress;
    private String faceImg;
    private String cardImg;
    private String cropCard;
    private String userAgent;
}


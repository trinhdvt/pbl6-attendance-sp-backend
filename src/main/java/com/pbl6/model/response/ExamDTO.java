package com.pbl6.model.response;

import com.pbl6.model.entity.Examination;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class ExamDTO {
    private int id;
    private String teacherName;
    private String examCode;
    private String detail;
    private Date openAt;
    private Date closeAt;

    public static ExamDTO toExamDTO(Examination exam) {
        if (exam == null) return null;

        return ExamDTO.builder()
                .id(exam.getId())
                .teacherName(exam.getCreator().getFullName())
                .examCode(exam.getExamCode())
                .detail(exam.getDetail())
                .openAt(exam.getOpenAt())
                .closeAt(exam.getCloseAt())
                .build();
    }
}
